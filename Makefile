all: arduinobot.o chatbot.o
		g++ arduinobot.o chatbot.o -o taskone -std=c++17


arduinobot.o: arduinobot.cpp
		g++ -c arduinobot.cpp -std=c++17

chatbot.o: chatbot.cpp
		g++ -c chatbot.cpp -std=c++17

clean:
		rm *.o taskone
