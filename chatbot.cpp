
#include <iostream>
#include <string>
#include <unistd.h>
#include "chatbot.h"
#include <vector>
#include <fstream>
#include <exception>
using namespace std;


// all functions included below

chatbot::chatbot() { //constructor of class

  // assigning values to variables declared in public class
  cout << "\n\t\t\t\tARDUINO BOT" << endl;
  usleep(2000000); // pauses compiler for 2 seconds
  fileName = ("searchKeywords.txt");
  fileName2 = ("hardwareKeywords.txt");
  fileName3 = ("softwareKeywords.txt");
}

string chatbot::getInput() { // this function takes in user iput, and then decides to or not to remove spaces.

  cout << "\n" << endl;
  std::string temp; // assign input to temp
  std::getline(std::cin, temp); // copies spaces as well as words to variable
  int size = temp.length(); // assigns size of string to variable
  char sentence[size]; // declares array of size size
  for (int x = 0; x <= size; x++) { // iterates through input and copies to array
    sentence[x] = temp[x];
  }
  removeSpaces(sentence); // calls remove spaces
  //cout << sentence; // prints no spaces version
  string newString(sentence); // converts returned array to a string

  return newString;

}

void chatbot::addToAllVectors(string concatString) { // this function inludes 2 nested functions
  bool xx = addToGeneralVector(concatString,generalKeywords); // adds general file contesnts to a vector
  bool xy = addToSearchVector(fileName,searchVector,concatString); // adds search file contents to a vector
}


void chatbot::removeSpaces(char * str) { // removes spaces from user input
  // To keep track of non-space character count
  int count = 0;
  // Traverse the given string. If current character
  // is not space, then place it at index 'count++'
  for (int i = 0; str[i]; i++)
    if (str[i] != ' ')
      str[count++] = str[i]; // here count is
  // incremented
  str[count] = '\0';
}





bool chatbot::addToSearchVector(string fileName, vector<string> searchVector, string concatString) { // function takes info from file and adds to vector
  try{
      ifstream file;
      file.open("searchKeywords.txt"); // in this function a file is converted into a vector, the vector is then searched for comparison with input
      string word;

      if (!file.is_open()) throw ReadFileException(); // if file isnt open - return false

      while (getline(file,word)) { // while the the getline function is putting file into vector
        searchVector.push_back(word); // add file element to vector
      }

      for (int i = 0; i < searchVector.size(); i++)
      {
        if (concatString.find(searchVector[i]) != string::npos){ // if return value is found inside the vector

          bool find = searchKeywords(searchSpecificVector,concatString); // if search keywords are found, function is called to find what they want to search
          if (find == false) {
            break;
          }else {
            return true;
          }
        }
      }
      return false;
    }catch(std::exception &e){
      std::cerr << e.what() << std::endl;
    }

    return true;
  }


bool chatbot::searchKeywords(vector<string> searchSpecificVector, string concatString) // function takes info from file and adds to vector

{

    ifstream file;
    string word;
    file.open("searchSpecificKeywords.txt"); // converts search specific words into vector

    if (!file.is_open()) return false; // if file doesnt open - return false

    while (getline(file,word)) {
      searchSpecificVector.push_back(word); // add to vector
    }

    for (int i = 0; i < searchSpecificVector.size(); i++) { // checks if input matches search specific words

      if (concatString.find(searchSpecificVector[i]) <= 100){ // if item inside vector matches return value
        keyword = searchSpecificVector[i];
        usleep(2000000);
        cout << "" << endl;
        cout << "                     " << searchSpecificVector[i+1] << endl; // print the output associated with keyword which relative to line n is n+1
        return true;
      }
    }
    return false;
  }




bool chatbot::addToGeneralVector(string concatString, vector<string> generalKeywords){
  try{
    tryAddToGeneralVector(concatString, generalKeywords);
  }catch(std::exception &e){
    std::cerr << e.what() << std::endl;
    return false;
  }
  return true;
}


void chatbot::tryAddToGeneralVector(string &concatString, vector<string> &generalKeywords) { // function takes info from file and adds to vector

      ifstream file;
      string word;

      file.open("generalKeywords.txt"); // converts search specific words into vector

      if (!file.is_open())
        throw ReadFileException();// if file doesnt open, return false

      while (getline(file,word))
      {
        generalKeywords.push_back(word);
        }

      for (int i = 0; i < generalKeywords.size(); i++) // checks if input matches search specific words
      {

        if (concatString.find(generalKeywords[i]) <= 100){ // if inside the vector a word matched a word in return value

          string keyword2 = generalKeywords[i];

          usleep(2000000);
          cout << "" << endl;
          cout << "                     " << generalKeywords[i+1] << endl; // print pre determined response

          return;
        }
      }
    }


bool chatbot::exitCriteria(string concatString) { // function to determine if user is ending conversation

  ifstream file;
  string word;
  file.open("exitCriteriaKeywords.txt"); // converts search specific words into vector

  if (!file.is_open()){
    return false;
  }
   //return false; // if file doesnt open, return false.

  while (getline(file,word))
  {
    exitCriteriaone.push_back(word); // add words from file to a vector
    }

  for (int i = 0; i < exitCriteriaone.size(); i++) // checks if input matches search specific words
  {
    if (concatString.find(exitCriteriaone[i]) <= 100){ // if inside the vector a word matched a word in return value
      return false;
    }
  }
return true;
}


void chatbot::repetitionCheck(string concatString) {

  if (previousInput == concatString) {	// check if word is same as the last
    cout << "\n" << endl;
    cout << "                     " << "Are you trying to break me ?, its getting kinda annoying ." << endl;
  }
    previousInput = concatString;
}


chatbot::~chatbot() { //destructor that is activated when exitCriteria is met or the program comes to a natural end.

  usleep(2000000); // delay compiler for 2 seconds
  cout << "\n\n" << "                     " << "I'm glad I could be of service" << endl;
  usleep(2000000);
  cout << "                     " <<"If you ever need help with again, be sure to contact us withought hesitation\n" << endl;
  usleep(2000000);
  cout << "                     " << "I will be back...\n" << endl;
  usleep(5000000); // sleep function
  cout << "                     " <<"Goodbye !\n\n" << endl;

}
