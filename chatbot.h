
#include <iostream>
#include <string>
#include <unistd.h>
#include <vector>
#include <exception>

using namespace std;

#pragma once


class ReadFileException: public std::exception {
  virtual const char *what() const noexcept override{
    return "Could not read from file";
  }
};


class chatbot {

public:

  // variables used in main and .cpp files

  string mainQuery;
  string topicSearch;
  string response;
  string keyword;
  string keyword2;
  string keyword3;
  string fileName;
  string fileName2;
  string fileName3;
  bool test;
  int total;
  string previousInput;

  // vectors used in main and .cpp files

  vector<string> hardwareVector;
  vector<string> softwareVector;
  vector<string> searchVector;
  vector<string> generalKeywords;
  vector<string> hardwareSpecificVector;
  vector<string> softwareSpecificVector;
  vector<string> searchSpecificVector;
  vector<string> exitCriteriaone;


  // functions used in main and .cpp files

  chatbot();
  string getInput();
  void removeSpaces(char *str);
  bool addToSearchVector(string fileName, vector<string> searchVector, string concatString);
  void addToHardwareVector(string fileName2, vector<string> hardwareVector, string concatString);
  void addToSoftwareVector(string fileName3, vector<string> softwareVector, string concatString);
  void addToAllVectors(string concatString);
  void findSearchKeyword(vector<string> searchVector, string concatString);
  bool searchKeywords(vector<string> searchVector, string concatString);
  void nonRecognise();
  bool addToGeneralVector(string concatString, vector<string> generalKeywords);
  void tryAddToGeneralVector(string &concatString, vector<string> &generalKeywords);
  bool exitCriteria(string concatString);
  void repetitionCheck(string concatString);
  ~chatbot();


private:




};
